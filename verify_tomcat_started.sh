#!/bin/sh

echo "verifying is tomcat up"
var=`netstat -tulpn | grep 8080 | wc -l`
if [ $var != 1 ]
then
echo "O/P : $var"
echo "Tomcat is not up on 8080"
exit 1
fi
exit 0
